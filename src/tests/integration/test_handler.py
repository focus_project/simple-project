async def test_hello(cli):
    resp = await cli.get('/')
    assert resp.status == 200
    assert await resp.text() == 'Hello, world'

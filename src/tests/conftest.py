import pytest
from aiohttp import web

from app.server import init_app


@pytest.fixture
def cli(loop, aiohttp_client):
    app = init_app()
    return loop.run_until_complete(aiohttp_client(app))

import aiohttp_cors

from app.api import handler


def setup_routes(app):
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
            )
    })

    # Add all resources to `CorsConfig`.
    resource = cors.add(app.router.add_resource("/"))
    cors.add(resource.add_route("GET", handler.hello))

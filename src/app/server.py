import sys

from aiohttp import web

from app.api.routes import setup_routes
from app.configurate import get_config


def init_app(argv):
    app = web.Application()
    app['config'] = get_config(argv)
    setup_routes(app)
    return app


if __name__ == "__main__":
    app = init_app(sys.argv[1:])
    web.run_app(app)

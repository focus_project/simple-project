import argparse
import pathlib

import trafaret as T
from trafaret_config import commandline

BASE_DIR = pathlib.Path(__file__).parent.parent
DEFAULT_CONFIG_PATH = BASE_DIR / 'config' / 'default.yaml'

TRAFARET_FOR_CONFIG = T.Dict({
    'service': T.Dict({
        'name': T.String,
        'version': T.String,
        'port': T.Int(gte=1024)
    })
})


def get_config(argv):
    ap = argparse.ArgumentParser()
    commandline.standard_argparse_options(
        ap,
        default_config=DEFAULT_CONFIG_PATH
    )
    options, unknown = ap.parse_known_args(argv)

    config = commandline.config_from_options(options, TRAFARET_FOR_CONFIG)

    return config
